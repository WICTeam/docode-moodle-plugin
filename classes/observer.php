<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in Docode Plagiarism plugin.
 *
 * @package   plagiarism_docode
 * @author	 Dan Marsden <dan@danmarsden.com>, Ramindu Deshapriya <rasade88@gmail.com>
 * @copyright  2011 Dan Marsden http://danmarsden.com
 * @copyright  2015 Ramindu Deshapriya <rasade88@gmail.com>
 * @license	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/plagiarism/docode/lib.php');

/**
 * Class plagiarism_urkund_observer
 *
 * @package   plagiarism_docode
 * @copyright 2017 Dan Marsden
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plagiarism_docode_observer {

	/**
	 * Observer function to handle the assessable_uploaded event in mod_assign.
	 * @param \assignsubmission_file\event\assessable_uploaded $event
	 */
	public static function assignsubmission_file_uploaded(\assignsubmission_file\event\assessable_uploaded $event) {
		global $CFG, $DB;
		$eventdata = $event->get_data();
		$userid = $eventdata["userid"];
		$cmid = $eventdata["contextinstanceid"];
		$pathnamehashes = $eventdata["other"]["pathnamehashes"];
		if (!plagiarism_plugin_docode::is_docode_available($cmid))
			return;
		$previous_record_hashes = array_keys($DB->get_records_menu("plagiarism_docode_files", array("userid" => $userid, "cmid" => $cmid), null, "filehash"));
		$old_hashes = array_diff($previous_record_hashes, $pathnamehashes);
		foreach ($pathnamehashes as $hash) {
			if (in_array($hash, $previous_record_hashes))
				continue;
			$record = new stdClass();
			$record->userid = $userid;
			$record->cmid = $cmid;
			$record->filehash = $hash;
			$record->analysisid = null;
			$record->reporturl = null;
			$record->plagiarismscore = null;
			$DB->insert_record('plagiarism_docode_files', $record, false);
		}
		foreach ($old_hashes as $old_hash)
			$DB->delete_records("plagiarism_docode_files", array("userid" => $userid, "cmid" => $cmid, "filehash" => $old_hash));
	}


	public static function assignment_deleted(\core\event\course_module_deleted $event) {
		global $DB;
		$eventdata = $event->get_data();
		$cmid = $eventdata["contextinstanceid"];
		if (!plagiarism_plugin_docode::is_docode_available($cmid)) {
			error_log("assignment_deleted doing nothing");
			return;
		}
		if ($eventdata["other"]["modulename"] != "assign")
			return;
		$DB->delete_records("plagiarism_docode_files", array("cmid" => $eventdata["contextinstanceid"]));
	}
}
