<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * DOCODE task - get scores
 *
 * @package    plagiarism_docode
 * @author     Dan Marsden http://danmarsden.com
 * @copyright  2017 Dan Marsden
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/plagiarism/lib.php');

defined('MOODLE_INTERNAL') || die();

/**
 * get_scores class, used to get scores for submitted files.
 *
 * @package    plagiarism_DOCODE
 * @author     Dan Marsden http://danmarsden.com
 * @copyright 2017 Dan Marsden
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plagiarism_docode_get_scores extends \core\task\scheduled_task {
    /**
     * Returns the name of this task.
     */
    public function get_name() {
        // Shown in admin screens.
        return get_string('getscores', 'plagiarism_docode');
    }

    /**
     * Executes task.
     */
    public function execute() {
        global $DB, $CFG;
        $plagiarism_config = (array)get_config("plagiarism");
        $docode_server = $plagiarism_config["docode_server"];
        $token = $plagiarism_config["docode_token"];
        $result = $DB->get_records_select('plagiarism_docode_files', "reporturl is null and analysisid is not null");
        foreach ($result as $analysis) {
            $c = new curl();
            $c->setHeader(array('Authorization: Token '.$token));
            $response = $c->get($docode_server."/app/api/analyses/".$analysis->analysisid);
            if ($c->info["http_code"] != 200)
                continue;
            $analysis_response = json_decode($response);
            $analysis_status = $analysis_response->status;
            $analysis_public_url = $analysis_response->public_url;
            if ($analysis_status >= 2) {
                $plagiarism_index = $analysis_response->result->suspectContainment;
                $record = new stdClass();
                $record->id = $analysis->id;
                $record->plagiarismscore = $plagiarism_index;
                if (!$analysis_public_url) {
                    $url = $docode_server."/app/api/analyses/".$analysis->analysisid;
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                    curl_setopt($curl, CURLOPT_POSTFIELDS,  array("is_public" => "true"));
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Token '.$token));
                    $new_response = curl_exec($curl);
                    if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200)
                        $record->reporturl = json_decode($new_response)->public_url;
                    curl_close($curl);
                }
                $lastinsertid = $DB->update_record('plagiarism_docode_files', $record, false);
            }
        }
    }
}
