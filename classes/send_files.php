<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * DOCODE task - Send queued files to docode.
 *
 * @package    plagiarism_docode
 * @author     Dan Marsden http://danmarsden.com
 * @copyright  2017 Dan Marsden
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

/**
 * send_files class, used to send queued files to DOCODE.
 *
 * @package    plagiarism_docode
 * @author     Dan Marsden http://danmarsden.com
 * @copyright  2017 Dan Marsden
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class plagiarism_docode_send_files extends \core\task\scheduled_task {
    /**
     * Returns the name of this task.
     */
    public function get_name() {
        // Shown in admin screens.
        return get_string('sendfiles', 'plagiarism_docode');;
    }

    /**
     * Execute task.
     */
    public function execute() {
        global $CFG, $DB;
        $fs = get_file_storage();
        $plagiarism_config = (array)get_config("plagiarism");
        $docode_server = $plagiarism_config["docode_server"];
        $docode_token = $plagiarism_config["docode_token"];
        $pending_files = $DB->get_records('plagiarism_docode_files', array("analysisid" => null));
        foreach ($pending_files as $pending_file) {
            $file = $fs->get_file_by_hash($pending_file->filehash);
            $c = new curl();
            $c->setHeader(array('Authorization: Token '.$docode_token));
            $first_response = $c->post($docode_server."/app/api/analyses", array("file"=>$file));
            if ($c->info["http_code"] != 201) {
                error_log("Could not create analysis");
                continue;
            }
            $create_analysis_response = json_decode($first_response);
            $analysis_id = $create_analysis_response->id;
            $c->post($docode_server."/app/api/analyses/".$analysis_id);
            if ($c->info["http_code"] != 200) {
                error_log("Could not start analysis");
                continue;
            }
            $pending_file->analysisid = $analysis_id;
            $DB->update_record('plagiarism_docode_files', $pending_file, false);
        }
    }
}
