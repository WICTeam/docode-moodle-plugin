<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * lib.php - Contains Plagiarism plugin specific functions called by Modules.
 *
 * @since 2.0
 * @package    plagiarism_docode
 * @subpackage plagiarism
 * @copyright  2010 Dan Marsden http://danmarsden.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

//get global class
global $CFG;
require_once($CFG->dirroot.'/plagiarism/lib.php');

class plagiarism_plugin_docode extends plagiarism_plugin {
     /**
     * hook to allow plagiarism specific information to be displayed beside a submission
     * @param array  $linkarraycontains all relevant information for the plugin to generate a link
     * @return string
     *
     */
    public function get_links($linkarray) {
        global $DB;
        //$userid, $file, $cmid, $course, $module
        $cmid = $linkarray['cmid'];
        $modname = get_coursemodule_from_id('', $cmid)->modname;
        if (!($modname === 'assign') or !plagiarism_plugin_docode::is_docode_available($cmid))
            return "";
        $userid = $linkarray['userid'];
        $file = $linkarray['file'];
        $analysis = $DB->get_record('plagiarism_docode_files', array('filehash'=>$file->get_pathnamehash()));
        if ($analysis && $analysis->plagiarismscore != null && $analysis->reporturl != null)
            $output = "<p><a href=\"".$analysis->reporturl."\" target=\"_blank\">".sprintf(get_string("microreport", "plagiarism_docode"), $analysis->plagiarismscore * 100)."</a></p>";
        else if (!$analysis)
			$output = "<p>".get_string("absentanalysis", "plagiarism_docode")."</p>";
		else
            $output = "<p>".get_string("pendinganalysis", "plagiarism_docode")."</p>";
        return $output;
    }


    /* hook to save plagiarism specific settings on a module settings page
     * @param object $data - data from an mform submission.
    */
    public function save_form_elements($data) {
        global $DB;
        $record = new stdClass();
        $cmid = $data->coursemodule;
        if ($data->use_docode) {
            $record->cmid = $cmid;
            $DB->insert_record('plagiarism_docode_modules', $record, false);
        }
        else
            $DB->delete_records('plagiarism_docode_modules', array('cmid' => $cmid));
    }

    /**
     * hook to add plagiarism specific settings to a module settings page
     * @param object $mform  - Moodle form
     * @param object $context - current context
     */
    public function get_form_elements_module($mform, $context, $modulename = "") {
        global $DB;
        $mform->addElement('header', 'plagiarismdesc', get_string('docode', 'plagiarism_docode'));
        $mform->addElement('checkbox', 'use_docode', get_string("usedocodeinmodule", "plagiarism_docode"));
        $mform->setDefault('use_docode', !empty($DB->get_records_menu("plagiarism_docode_modules", array("cmid" => optional_param('update', 0, PARAM_INT))))); // We get the cmid from the url, keyword "update"
    }

    /**
     * hook to allow a disclosure to be printed notifying users what will happen with their submission
     * @param int $cmid - course module id
     * @return string
     */
    public function print_disclosure($cmid) {
        global $OUTPUT;
        $plagiarismsettings = (array)get_config('plagiarism');
        if (!plagiarism_plugin_docode::is_docode_available($cmid))
            return;
        echo $OUTPUT->box_start('generalbox boxaligncenter', 'intro');
        $formatoptions = new stdClass;
        $formatoptions->noclean = true;
        echo format_text(get_string('studentdisclosure', 'plagiarism_docode'), FORMAT_MOODLE, $formatoptions);
        echo $OUTPUT->box_end();
    }

    /**
     * hook to allow status of submitted files to be updated - called on grading/report pages.
     *
     * @param object $course - full Course object
     * @param object $cm - full cm object
     */
    public function update_status($course, $cm) {
       return '';
    }

    public static function is_docode_available($cmid) {
        global $DB;
        $settings = (array)get_config('plagiarism');
        $cmid_availability = $DB->get_records_menu("plagiarism_docode_modules", array("cmid" => $cmid));
        $is_docode_available = array_key_exists("docode_use", $settings) && $settings["docode_use"] && $cmid_availability;
        if ($is_docode_available)
            error_log("Available docode");
        else
            error_log("Not available");
        return $is_docode_available;
    }
}
