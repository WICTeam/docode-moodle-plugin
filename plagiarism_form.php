<?php

require_once($CFG->dirroot.'/lib/formslib.php');

class plagiarism_setup_form extends moodleform {
    function definition () {
        global $CFG;
        $mform =& $this->_form;
        $mform->addElement('html', get_string('docodeexplain', 'plagiarism_docode'));
        $mform->addElement('checkbox', 'docode_use', get_string('usedocode', 'plagiarism_docode'));
        $mform->addElement('text', 'docode_token', get_string('token','plagiarism_docode'));
        $mform->addElement('text', 'docode_server', get_string('docode_server','plagiarism_docode'));
        $mform->setDefault('docode_server', "https://docode.cl");
        $mform->addHelpButton('docode_server', 'docode_server', 'plagiarism_docode');
        $mform->setType('docode_server', PARAM_TEXT);
        $mform->setType('docode_token', PARAM_TEXT);
        $this->add_action_buttons(true);
    }
}
