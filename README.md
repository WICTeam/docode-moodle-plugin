README
-------

Docode Plugin for Moodle

### Installation ###

All these steps have to be done by the admin user.

 * Acquire a user token from the DOCODE team.
 
 * Clone this repository to moodle's plagiarism plugin folder, usually ```plagiarism``` and rename it to ```docode```.
 
 * Go to Site administration -> Advanced features and check the "Enable plagiarism plugins" checkbox.
 
 * Go to Site administration -> Plugins -> Plagiarism -> DOCODE plagiarism plugin, check the "Enable DOCODE" checkbox and put the user token, without any spaces.



