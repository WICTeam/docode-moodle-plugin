<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   plagiarism_docode
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['studentdisclosure']  ='Los documentos enviados serán analizados con DOCODE';
$string['docode'] = 'DOCODE plagiarism plugin';
$string['pluginname'] = 'Plugin de plagio de DOCODE';
$string['token'] = 'Token de usuario';
$string['usedocode'] ='Activar DOCODE';
$string['usedocodeinmodule'] ='Usar DOCODE en este módulo';
$string['docode_server'] ='URL base de DOCODE';
$string['docode_server_help'] ='URL base de DOCODE, incluyendo protocolo (http o https)';
$string['savedconfigsuccess'] = 'Configuración de DOCODE guardada';
$string['docodeexplain'] = 'Para más información sobre este plugin vea: <a href="http://docode.cl" target="_blank">http://docode.cl</a>';
$string['microreport'] = 'DOCODE: %0.2f%% de plagio';
$string['pendinganalysis'] = 'Análisis no está listo';
$string['absentanalysis'] = 'El documento no ha sido enviado a DOCODE';
